export default {
  namespaced: true,
  state: {
    hitungan: 0,
  },
  getters: {
    hitungan: state => state.hitungan,
  },
  mutations: {
    kenaikan: (state, payload) => {
      state.hitungan += payload
    },
  },
  actions: {},
}
