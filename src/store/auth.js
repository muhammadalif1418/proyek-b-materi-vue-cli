import axios from 'axios'

export default {
  namespaced: true,
  state: {
    token: '',
    user: {},
  },
  mutations: {
    setToken: (state, payload) => {
      state.token = payload
    },
    setUser: (state, payload) => {
      state.user = payload
    },
  },
  actions: {
    setToken: ({commit, dispatch}, payload) => {
      commit('setToken', payload)

      dispatch('checkToken', payload)
    },
    checkToken: ({commit}, payload) => {
      const konfigurasi = {
        method: 'post',
        url: 'https://demo-api-vue.sanbercloud.com/api/v2/auth/me',
        headers: {
          'Authorization': 'Bearer ' + payload,
        },
      }

      axios(konfigurasi)
          .then(respons => {
            commit('setUser', respons.data)
          })
          .catch(() => {
            commit('setUser', {})
            commit('setToken', '')
          })
    },
    setUser: ({commit}, payload) => {
      commit('setUser', payload)
    },
  },
  getters: {
    user: state => state.user,
    token: state => state.token,
    tamu: state => Object.keys(state.user).length === 0,
  },
}
