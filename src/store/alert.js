export default {
  namespaced: true,
  state: {
    status: false,
    warna: 'primer',
    teks: '',
  },
  mutations: {
    set: (state, payload) => {
      state.status = payload.status
      state.warna = payload.warna
      state.teks = payload.teks
    },
  },
  actions: {
    set: ({commit}, payload) => {
      commit('set', payload)
    },
  },
  getters: {
    status: state => state.status,
    warna: state => state.warna,
    teks: state => state.teks,
  },
}
