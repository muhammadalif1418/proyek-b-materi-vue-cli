export default {
  namespaced: true,
  state: {
    status: false,
    komponen: '',
    parameter: {},
  },
  mutations: {
    setStatus: (state, status) => {
      state.status = status
    },
    setKomponen: (state, {komponen, parameter}) => {
      state.komponen = komponen
      state.parameter = parameter
    },
  },
  actions: {
    setStatus: ({commit}, status) => {
      commit('setStatus', status)
    },
    setKomponen: ({commit}, {komponen, parameter}) => {
      commit('setKomponen', {komponen, parameter})
      commit('setStatus', true)
    },
  },
  getters: {
    status: state => state.status,
    komponen: state => state.komponen,
    parameter: state => state.parameter,
  },
}
